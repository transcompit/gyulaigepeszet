package com.transfet.gepeszet.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transfet.gepeszet.persistence.entity.TenderItemEntity;

@Repository
public interface TenderItemRepository extends JpaRepository<TenderItemEntity, Long> {
}
