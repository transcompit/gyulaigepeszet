package com.transfet.gepeszet.persistence.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "BASE_PRICE")
    private BigDecimal basePrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public static ProductEntityBuilder builder() {
        return new ProductEntityBuilder();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductEntity{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", basePrice=").append(basePrice);
        sb.append('}');
        return sb.toString();
    }

    public static final class ProductEntityBuilder {
        private Long id;
        private String name;
        private BigDecimal basePrice;

        private ProductEntityBuilder() {
        }

        public ProductEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ProductEntityBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductEntityBuilder basePrice(BigDecimal basePrice) {
            this.basePrice = basePrice;
            return this;
        }

        public ProductEntity build() {
            ProductEntity productEntity = new ProductEntity();
            productEntity.setId(id);
            productEntity.setName(name);
            productEntity.setBasePrice(basePrice);
            return productEntity;
        }
    }
}
