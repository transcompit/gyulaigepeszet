package com.transfet.gepeszet.persistence.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TENDER_ITEM")
public class TenderItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private BigDecimal nettoPrice;

    private BigDecimal bruttoPrice;
    /*@ManyToOne
    @JoinTable(name = "tender_item_product_sw", inverseJoinColumns = @JoinColumn(name = "tender_item_id", referencedColumnName = "id"),
        joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
    private ProductEntity product;*/

    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   /* public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }*/

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(BigDecimal nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public BigDecimal getBruttoPrice() {
        return bruttoPrice;
    }

    public void setBruttoPrice(BigDecimal bruttoPrice) {
        this.bruttoPrice = bruttoPrice;
    }

    public static TenderItemEntityBuilder builder() {
        return new TenderItemEntityBuilder();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TenderItemEntity{");
        sb.append("id=").append(id);
        // sb.append(", product=").append(product);
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }

    public static final class TenderItemEntityBuilder {
        private Long id;
        //private ProductEntity product;
        private Long count;
        private BigDecimal nettoPrice;
        private BigDecimal bruttoPrice;
        private String name;

        private TenderItemEntityBuilder() {
        }

        public TenderItemEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        /*public TenderItemEntityBuilder product(ProductEntity product) {
            this.product = product;
            return this;
        }*/

        public TenderItemEntityBuilder nettoPrice(BigDecimal nettoPrice) {
            this.nettoPrice = nettoPrice;
            return this;
        }

        public TenderItemEntityBuilder bruttoPrice(BigDecimal bruttoPrice) {
            this.bruttoPrice = bruttoPrice;
            return this;
        }

        public TenderItemEntityBuilder name(String name) {
            this.name = name;
            return this;
        }

        public TenderItemEntityBuilder count(Long count) {
            this.count = count;
            return this;
        }

        public TenderItemEntity build() {
            TenderItemEntity tenderItemEntity = new TenderItemEntity();
            tenderItemEntity.setId(id);
            //tenderItemEntity.setProduct(product);
            tenderItemEntity.setCount(count);
            tenderItemEntity.setName(name);
            tenderItemEntity.setBruttoPrice(bruttoPrice);
            tenderItemEntity.setNettoPrice(nettoPrice);
            return tenderItemEntity;
        }
    }
}
