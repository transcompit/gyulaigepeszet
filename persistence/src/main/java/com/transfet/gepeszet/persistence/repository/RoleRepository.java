package com.transfet.gepeszet.persistence.repository;

import com.transfet.gepeszet.persistence.entity.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for user roles.
 */
@Repository
public interface RoleRepository extends JpaRepository<UserRoleEntity, Long> {

    UserRoleEntity findByRoleName(String roleName);
}
