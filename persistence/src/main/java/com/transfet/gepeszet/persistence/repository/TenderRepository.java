package com.transfet.gepeszet.persistence.repository;

import com.transfet.gepeszet.persistence.entity.TenderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenderRepository extends JpaRepository<TenderEntity, Long> {

}
