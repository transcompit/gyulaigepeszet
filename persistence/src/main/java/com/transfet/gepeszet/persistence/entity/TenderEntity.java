package com.transfet.gepeszet.persistence.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "TENDER")
public class TenderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tender_tender_item_sw", inverseJoinColumns = @JoinColumn(name = "tender_item_id", referencedColumnName = "id"),
        joinColumns = @JoinColumn(name = "tender_id", referencedColumnName = "id"))
    private List<TenderItemEntity> items;

    @Column(name = "NETTO_PRICE")
    private BigDecimal nettoPrice;

    @Column(name = "BRUTTO_PRICE")
    private BigDecimal bruttoPrice;

    @Lob
    @Column(name = "TENDER")
    private byte[] tender;

    private String createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TenderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<TenderItemEntity> items) {
        this.items = items;
    }

    public BigDecimal getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(BigDecimal nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public BigDecimal getBruttoPrice() {
        return bruttoPrice;
    }

    public void setBruttoPrice(BigDecimal bruttoPrice) {
        this.bruttoPrice = bruttoPrice;
    }

    public byte[] getTender() {
        return tender;
    }

    public void setTender(byte[] tender) {
        this.tender = tender;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public static TenderEntityBuilder builder() {
        return new TenderEntityBuilder();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TenderEntity{");
        sb.append("id=").append(id);
        sb.append(", description='").append(description).append('\'');
        sb.append(", items=").append(items);
        sb.append(", nettoPrice=").append(nettoPrice);
        sb.append(", bruttoPrice=").append(bruttoPrice);
        sb.append(", createdBy=").append(createdBy);
        sb.append('}');
        return sb.toString();
    }

    public static final class TenderEntityBuilder {
        private Long id;
        private String description;
        private List<TenderItemEntity> items;
        private BigDecimal nettoPrice;
        private BigDecimal bruttoPrice;
        private String createdBy;

        private TenderEntityBuilder() {
        }

        public TenderEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public TenderEntityBuilder description(String description) {
            this.description = description;
            return this;
        }

        public TenderEntityBuilder items(List<TenderItemEntity> items) {
            this.items = items;
            return this;
        }

        public TenderEntityBuilder nettoPrice(BigDecimal nettoPrice) {
            this.nettoPrice = nettoPrice;
            return this;
        }

        public TenderEntityBuilder bruttoPrice(BigDecimal bruttoPrice) {
            this.bruttoPrice = bruttoPrice;
            return this;
        }

        public TenderEntityBuilder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public TenderEntity build() {
            TenderEntity tenderEntity = new TenderEntity();
            tenderEntity.setId(id);
            tenderEntity.setDescription(description);
            tenderEntity.setItems(items);
            tenderEntity.setNettoPrice(nettoPrice);
            tenderEntity.setBruttoPrice(bruttoPrice);
            tenderEntity.setCreatedBy(createdBy);
            return tenderEntity;
        }
    }
}
