INSERT INTO USER(first_name, last_name, password, user_name) values ('admin', 'admin', '$2a$10$tX5eQXWniibnIq3QYqDh5Ou/ZUjk26STTplxqsNeHgkDXgHriADuW', 'admin');

INSERT INTO USER_ROLE (role_name) values('ROLE_ADMIN');

INSERT INTO USER_ROLE_SW(user_id, role_id) values(1,1);