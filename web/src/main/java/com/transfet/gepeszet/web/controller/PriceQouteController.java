package com.transfet.gepeszet.web.controller;

import com.transfet.gepeszet.service.tender.TenderService;
import com.transfet.gepeszet.service.tender.domain.CreatePriceQouteRequest;
import com.transfet.gepeszet.service.tender.domain.PriceQouteItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.LinkedList;
import java.util.Objects;

@Controller
public class PriceQouteController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PriceQouteController.class);
    private static final String AJAX_HEADER_NAME = "X-Requested-With";
    private static final String AJAX_HEADER_VALUE = "XMLHttpRequest";
    private static final String CREATE_ENDPOINT = "/qoute/create";
    private static final String CREATE_PRICE_QOUTE_VIEW = "pricequote/createPriceQoute";

    private TenderService tenderService;

    @Autowired
    public PriceQouteController(TenderService tenderService) {
        this.tenderService = tenderService;
    }

    @GetMapping(CREATE_ENDPOINT)
    public String getCreationPage(Model model) {
        CreatePriceQouteRequest createPriceQouteRequest = new CreatePriceQouteRequest(new LinkedList<>());
        model.addAttribute(createPriceQouteRequest);

        return CREATE_PRICE_QOUTE_VIEW;
    }

    @PostMapping(params = "save", path = CREATE_ENDPOINT)
    public String createTender(CreatePriceQouteRequest createPriceQouteRequest, Model model, Principal principal) {

        String createdBy = principal.getName();
        LOGGER.info(createdBy);
        createPriceQouteRequest.setCreatedBy(createdBy);
        byte[] result = tenderService.createTender(createPriceQouteRequest);
        LOGGER.info("tender{}", result);

        String base64Result = Base64Utils.encodeToString(result);

        model.addAttribute("document", base64Result);
        return CREATE_PRICE_QOUTE_VIEW + "::#documentModalHolder";
    }

    @PostMapping(params = "addItem", path = CREATE_ENDPOINT)
    public String addPriceQouteItem(CreatePriceQouteRequest createPriceQouteRequest, HttpServletRequest request) {
        LOGGER.info("Add price qoute item request arrived with request = {}", createPriceQouteRequest);

        String responseView;

        if (Objects.isNull(createPriceQouteRequest.getItems())) {
            createPriceQouteRequest.setItems(new LinkedList<>());
        }

        createPriceQouteRequest.getItems().add(new PriceQouteItem());

        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            responseView = CREATE_PRICE_QOUTE_VIEW + "::#items";
        } else {
            responseView = CREATE_PRICE_QOUTE_VIEW;
        }

        return responseView;
    }

    @PostMapping(params = "removeItem", path = CREATE_ENDPOINT)
    public String removePriceQouteItem(CreatePriceQouteRequest createPriceQouteRequest, HttpServletRequest request, @RequestParam("removeItem") int index) {
        LOGGER.info("Remove price qoute item request arrived with request = {} and index ={}", createPriceQouteRequest, index);

        String responseView;
        createPriceQouteRequest.getItems().remove(index);

        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            responseView = CREATE_PRICE_QOUTE_VIEW + "::#items";
        } else {
            responseView = CREATE_PRICE_QOUTE_VIEW;
        }

        return responseView;
    }
}
