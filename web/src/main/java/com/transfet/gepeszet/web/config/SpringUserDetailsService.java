package com.transfet.gepeszet.web.config;

import com.transfet.gepeszet.service.user.UserService;
import com.transfet.gepeszet.service.user.domain.Role;
import com.transfet.gepeszet.service.user.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class SpringUserDetailsService implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringUserDetailsService.class);

    private UserService userService;

    @Autowired
    public SpringUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Optional<User> user = userService.findByUserName(userName);
        org.springframework.security.core.userdetails.User authenticatedUser;

        LOGGER.info("user details for =" , user);
        if (user.isPresent()) {

            Set<GrantedAuthority> authorities = buildUserAuthorities(user.get().getRoles());
            authenticatedUser = buildUserForAuthentication(user.get(), authorities);

        } else {

            throw new UsernameNotFoundException(userName);
        }

        return authenticatedUser;
    }

    private org.springframework.security.core.userdetails.User buildUserForAuthentication(User user, Set<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(
                user.getUserName(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                authorities);
    }

    private Set<GrantedAuthority> buildUserAuthorities(Set<Role> roles) {
        return roles.stream()
                .map(Role::getRoleName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}
