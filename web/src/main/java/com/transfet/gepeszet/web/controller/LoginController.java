package com.transfet.gepeszet.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for login requests and responses.
 */
@Controller
public class LoginController {

    private static final String BASE_ENDPOINT = "/admin";
    private static final String LOGIN_ENDPOINT = "/login";
    public static final String ACCESS_DENIED_ENDPOINT = "/access-denied";

    @GetMapping(BASE_ENDPOINT)
    public ModelAndView getIndexPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        return modelAndView;
    }

    @GetMapping(LOGIN_ENDPOINT)
    public ModelAndView getLoginPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        return modelAndView;
    }

    @GetMapping(ACCESS_DENIED_ENDPOINT)
    public ModelAndView getAccessDeniedPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/error/access-denied");

        return modelAndView;
    }
}
