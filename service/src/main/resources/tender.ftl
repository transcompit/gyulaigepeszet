<html>
<head>
	<title>Árajánlat</title>

	<style>
		.table{
			width: 100%;
			padding-left: 15px;
			padding-right: 15px;
			text-align: center;
			border-collapse: collapse;
		}

		td, th {
			border: 1px solid black;
		}

		th{
			height: 30px;
		}

		img{
			max-width: 100px;
			max-height: 100px;
			padding-top: 20px;
			padding-left: 20px;
		}
	</style>
</head>
<body>
<table class="table">
	<tbody>
	<tr>
		<td style="width: 100%%;border: none; text-align: center">
			<img src="${logo}" alt="logoo"/>
		</td>
	</tr>
	<tr style="vertical-align: baseline;">
		<td style="width: 33.33333%;border: none;">
		</td>
		<td style="width: 33.33333%;border: none;">
			<h1 style="text-align: center;">Árajánlat</h1>
		</td>
		<td style="width: 33.33333%;border: none;">
			<p style="text-align: right; font-weight: bold; font-size: 14px">Árajánlatott készítette: ${username}</p>
		</td>
	</tr>
	</tbody>
</table>
<h2 style="text-align: center;">${description}</h2>

<hr/>
<table class="table">
	<thead>
	<tr>
		<th class="text-center">
			Termék név
		</th>
		<th class="text-center">
			Darabszám
		</th>
		<th class="text-center">
			Nettó egység ár
		</th>
		<th class="text-center">
			Bruttó egység ár
		</th>

	</tr>
	</thead>
	<tbody id="items">
	<#list items as item>
		<tr>
			<td>
				${item.name}
			</td>
			<td>
				${item.count}
			</td>
			<td style="text-align: right;">
				${item.nettoPrice} Ft
			</td>
			<td style="text-align: right;">
				${item.bruttoPrice} Ft
			</td>
		</tr>
	</#list>
	<tr>
		<td style="border: none;">
		</td>
		<td style="border: none;">
		</td>
		<td style="text-align: right;">
			Összesen nettó: ${nettoPrice} Ft
		</td>
		<td style="text-align: right;">
			Összesen bruttó: ${bruttoPrice} Ft
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>