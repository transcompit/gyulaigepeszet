package com.transfet.gepeszet.service.user.impl;

import com.transfet.gepeszet.persistence.entity.UserEntity;
import com.transfet.gepeszet.persistence.entity.UserRoleEntity;
import com.transfet.gepeszet.persistence.repository.RoleRepository;
import com.transfet.gepeszet.persistence.repository.UserRepository;
import com.transfet.gepeszet.service.user.UserService;
import com.transfet.gepeszet.service.user.domain.User;
import com.transfet.gepeszet.service.user.mapper.UserRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.nonNull;

/**
 * Implementation of {@link UserService}.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private UserRoleMapper userRoleMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, UserRoleMapper userRoleMapper) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRoleMapper = userRoleMapper;
    }

    @Override
    public void createUser(User user) {

        UserRoleEntity roleEntity = roleRepository.findByRoleName("ROLE_ADMIN");

        UserEntity userEntity = UserEntity.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .userName(user.getUserName())
                .roles(Collections.singleton(roleEntity))
                .build();

        userRepository.save(userEntity);

    }

    @Override
    public Optional<User> findByUserName(String userName) {

        UserEntity userEntity = userRepository.findByUserName(userName);
        User user = null;

        if (nonNull(userEntity)) {
            user = User.builder()
                    .userName(userEntity.getUserName())
                    .firstName(userEntity.getFirstName())
                    .lastName(userEntity.getLastName())
                    .password(userEntity.getPassword())
                    .roles(userRoleMapper.mapToRoleSet(userEntity.getRoles()))
                    .build();
        }

        LOGGER.info("find user = {}",user);
        return Optional.ofNullable(user);
    }
}
