package com.transfet.gepeszet.service.tender.document.impl;

import com.transfet.gepeszet.persistence.entity.TenderEntity;
import com.transfet.gepeszet.persistence.repository.TenderRepository;
import com.transfet.gepeszet.service.tender.document.TenderDocumentGenerator;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Service
public class TenderDocumentGeneratorImpl implements TenderDocumentGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TenderDocumentGeneratorImpl.class);
    private TenderRepository tenderRepository;

    @Autowired
    public TenderDocumentGeneratorImpl(TenderRepository tenderRepository) {
        this.tenderRepository = tenderRepository;
    }

    @Transactional
    @Override
    public byte[] generateTenderDocument(Long tenderId) {

        LOGGER.info("Tender document generation started with tender id={}", tenderId);

        byte[] generatedTender = null;
        Configuration cfg = new Configuration(new Version("2.3.23"));
        cfg.setClassForTemplateLoading(TenderDocumentGeneratorImpl.class, "/");
        cfg.setDefaultEncoding("UTF-8");


        try {

            TenderEntity tenderEntity = tenderRepository.findById(tenderId).get();
            LOGGER.info("Tender document generation started with tender={}", tenderEntity);


            Template template = cfg.getTemplate("tender.ftl");

            LOGGER.info("Tender document generated result={}", template);

            Map<String, Object> templateData = new HashMap<>();
            templateData.put("bruttoPrice", tenderEntity.getBruttoPrice());
            templateData.put("nettoPrice", tenderEntity.getNettoPrice());
            templateData.put("description", tenderEntity.getDescription());
            templateData.put("items", tenderEntity.getItems());
            templateData.put("username", tenderEntity.getCreatedBy());

            byte[] logoByte = getImageAsByteArray("/logo.jpg");
            String base64Logo = Base64Utils.encodeToString(logoByte);

            LOGGER.info(base64Logo);
            templateData.put("logo", "data:image/png;base64," + base64Logo);

            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                StringWriter stringWriter = new StringWriter();
                template.process(templateData, stringWriter);
                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocumentFromString(stringWriter.toString());
                renderer.layout();
                renderer.createPDF(outputStream);
                outputStream.close();

                generatedTender = outputStream.toByteArray();

                LOGGER.info("Tender document generated result={}", generatedTender);

                tenderEntity.setTender(generatedTender);
            } catch (Exception e) {
                LOGGER.info("Error in creating Tender={}", e);

            }

        } catch (Exception e) {
            LOGGER.info("Error in creating Tender={}", e);
        }
        return generatedTender;
    }

    private byte[] getImageAsByteArray(String imageName) {

        try {
            InputStream imgPath = TenderDocumentGeneratorImpl.class.getClassLoader().getResourceAsStream("logo.jpg");
            BufferedImage bufferedImage = ImageIO.read(imgPath);

            bufferedImage = Scalr.resize(bufferedImage, 128, 128);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", bos);
            byte[] byteImage = bos.toByteArray();
            return byteImage;

        } catch (IOException e) {
            LOGGER.error("IOException while getting image with image name ={}", imageName);
        }

        return null;
    }
}
