package com.transfet.gepeszet.service.tender;

import com.transfet.gepeszet.service.tender.domain.CreatePriceQouteRequest;

public interface TenderService {

    byte[] createTender(CreatePriceQouteRequest priceQouteRequest);
}
