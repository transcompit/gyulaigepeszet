package com.transfet.gepeszet.service.user;

import com.transfet.gepeszet.service.user.domain.User;

import java.util.Optional;

/**
 * Interface for users.
 */
public interface UserService {

    void createUser(User user);

    Optional<User> findByUserName(String userName);
}
