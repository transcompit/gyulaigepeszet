package com.transfet.gepeszet.service.tender.domain;

import java.math.BigDecimal;

public class PriceQouteItem {

    //private Product product;

    private Long count;
    private String name;

    private BigDecimal nettoPrice;
    private BigDecimal bruttoPrice;

    public PriceQouteItem() {
        //this.product = new Product();
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    /*public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(BigDecimal nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public BigDecimal getBruttoPrice() {
        return bruttoPrice;
    }

    public void setBruttoPrice(BigDecimal bruttoPrice) {
        this.bruttoPrice = bruttoPrice;
    }
}

