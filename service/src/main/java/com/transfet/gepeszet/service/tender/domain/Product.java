package com.transfet.gepeszet.service.tender.domain;

import java.math.BigDecimal;

public class Product {

    private String name;

    private BigDecimal nettoPrice;

    private BigDecimal bruttoPrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(BigDecimal nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public BigDecimal getBruttoPrice() {
        return bruttoPrice;
    }

    public void setBruttoPrice(BigDecimal bruttoPrice) {
        this.bruttoPrice = bruttoPrice;
    }


    public static ProductBuilder builder() {
        return new ProductBuilder();
    }

    public static final class ProductBuilder {
        private String name;
        private BigDecimal nettoPrice;
        private BigDecimal bruttoPrice;

        private ProductBuilder() {
        }

        public ProductBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder nettoPrice(BigDecimal nettoPrice) {
            this.nettoPrice = nettoPrice;
            return this;
        }

        public ProductBuilder bruttoPrice(BigDecimal bruttoPrice) {
            this.bruttoPrice = bruttoPrice;
            return this;
        }

        public Product build() {
            Product product = new Product();
            product.setName(name);
            product.setNettoPrice(nettoPrice);
            product.setBruttoPrice(bruttoPrice);
            return product;
        }
    }
}
