package com.transfet.gepeszet.service.user.mapper;

import com.transfet.gepeszet.persistence.entity.UserRoleEntity;
import com.transfet.gepeszet.service.user.domain.Role;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserRoleMapper {

    public Role mapToRole(UserRoleEntity userRoleEntity) {
        return new Role(userRoleEntity.getRoleName());
    }

    public Set<Role> mapToRoleSet(Set<UserRoleEntity> roleEntities) {

        return roleEntities.stream()
                .map(UserRoleEntity::getRoleName)
                .map(Role::new)
                .collect(Collectors.toSet());
    }
}
