package com.transfet.gepeszet.service.tender.mapper;

import com.transfet.gepeszet.persistence.entity.TenderEntity;
import com.transfet.gepeszet.persistence.entity.TenderItemEntity;
import com.transfet.gepeszet.persistence.repository.ProductRepository;
import com.transfet.gepeszet.persistence.repository.TenderItemRepository;
import com.transfet.gepeszet.service.tender.domain.CreatePriceQouteRequest;
import com.transfet.gepeszet.service.tender.domain.PriceQouteItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TenderMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(TenderMapper.class);
    private ProductRepository productRepository;
    private TenderItemRepository tenderItemRepository;

    @Autowired
    public TenderMapper(ProductRepository productRepository, TenderItemRepository tenderItemRepository) {
        this.productRepository = productRepository;
        this.tenderItemRepository = tenderItemRepository;
    }

    public TenderItemEntity mapToTenderItemEntity(PriceQouteItem priceQouteItem) {

       /* Optional<ProductEntity> product = productRepository.findById(1L);

        LOGGER.info("product={}", productRepository.findById(1L));
        TenderItemEntity tenderItemEntity = TenderItemEntity.builder()
            .count(priceQouteItem.getCount())
            .product(product.get())
            .build();*/

        TenderItemEntity tenderItemEntity = TenderItemEntity.builder()
                .count(priceQouteItem.getCount())
                .nettoPrice(priceQouteItem.getNettoPrice())
                .bruttoPrice(priceQouteItem.getBruttoPrice())
                .name(priceQouteItem.getName())
                .build();

        return tenderItemRepository.saveAndFlush(tenderItemEntity);
    }

    public List<TenderItemEntity> mapToTenderEntities(List<PriceQouteItem> priceQouteItems) {
        return priceQouteItems.stream()
                .map(this::mapToTenderItemEntity)
                .collect(Collectors.toList());
    }

    public TenderEntity mapToTenderEntity(CreatePriceQouteRequest request) {

        /*BigDecimal bruttoPrice = request.getItems()
            .stream()
            .map(priceQouteItem -> priceQouteItem.getProduct().getBruttoPrice().multiply(BigDecimal.valueOf(priceQouteItem.getCount())))
            .reduce(BigDecimal::add)
            .orElse(BigDecimal.ZERO);

        BigDecimal nettoPrice = request.getItems()
            .stream()
            .map(priceQouteItem -> priceQouteItem.getProduct().getNettoPrice().multiply(BigDecimal.valueOf(priceQouteItem.getCount())))
            .reduce(BigDecimal::add)
            .orElse(BigDecimal.ZERO);*/
        BigDecimal bruttoPrice = request.getItems()
                .stream()
                .map(priceQouteItem -> priceQouteItem.getBruttoPrice().multiply(BigDecimal.valueOf(priceQouteItem.getCount())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);

        BigDecimal nettoPrice = request.getItems()
                .stream()
                .map(priceQouteItem -> priceQouteItem.getNettoPrice().multiply(BigDecimal.valueOf(priceQouteItem.getCount())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);

        return TenderEntity.builder()
                .nettoPrice(nettoPrice)
                .bruttoPrice(bruttoPrice)
                .items(mapToTenderEntities(request.getItems()))
                .description(request.getDescription())
                .createdBy(request.getCreatedBy())
                .build();

    }
}
