package com.transfet.gepeszet.service.tender.domain;

import java.util.List;

public class CreatePriceQouteRequest {

    private String description;

    private List<PriceQouteItem> items;

    private String createdBy;

    public CreatePriceQouteRequest() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CreatePriceQouteRequest(List<PriceQouteItem> items) {
        this.items = items;
    }

    public List<PriceQouteItem> getItems() {
        return items;
    }

    public void setItems(List<PriceQouteItem> items) {
        this.items = items;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
