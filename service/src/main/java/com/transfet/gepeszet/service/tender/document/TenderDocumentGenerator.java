package com.transfet.gepeszet.service.tender.document;

public interface TenderDocumentGenerator {

    byte[] generateTenderDocument(Long tenderId);
}
