package com.transfet.gepeszet.service.tender.impl;

import com.transfet.gepeszet.persistence.entity.TenderEntity;
import com.transfet.gepeszet.persistence.repository.TenderRepository;
import com.transfet.gepeszet.service.tender.TenderService;
import com.transfet.gepeszet.service.tender.document.TenderDocumentGenerator;
import com.transfet.gepeszet.service.tender.domain.CreatePriceQouteRequest;
import com.transfet.gepeszet.service.tender.mapper.TenderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link TenderService}.
 */
@Service
public class TenderServiceImpl implements TenderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TenderServiceImpl.class);
    private TenderMapper tenderMapper;
    private TenderRepository tenderRepository;
    private TenderDocumentGenerator documentGenerator;

    @Autowired
    public TenderServiceImpl(TenderMapper tenderMapper, TenderRepository tenderRepository, TenderDocumentGenerator documentGenerator) {

        this.tenderMapper = tenderMapper;
        this.tenderRepository = tenderRepository;
        this.documentGenerator = documentGenerator;
    }

    @Transactional
    @Override
    public byte[] createTender(CreatePriceQouteRequest priceQouteRequest) {
        LOGGER.info("Tender request with request = {}", priceQouteRequest);

        TenderEntity tenderEntity = tenderMapper.mapToTenderEntity(priceQouteRequest);
        TenderEntity createdTender = createTenderEntity(tenderEntity);

        byte[] createdDocument = documentGenerator.generateTenderDocument(createdTender.getId());

        LOGGER.info("Created document = {}", createdDocument);

        return createdDocument;
    }

    @Transactional
    public TenderEntity createTenderEntity(TenderEntity tenderEntity) {
        return tenderRepository.save(tenderEntity);
    }
}
