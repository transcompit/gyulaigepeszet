FROM adoptopenjdk/openjdk11:latest

EXPOSE 8080

RUN mkdir /opt/gepeszet
COPY /web/target/web-1.0-SNAPSHOT.jar /opt/gepeszet

CMD ["java", "-jar", "/opt/gepeszet/web-1.0-SNAPSHOT.jar"]